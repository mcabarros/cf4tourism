# CF4Tourism

CF4Tourism is a simple session based collaborative filtering framework developed for the RecSys Challenge 2019, for ranking a list of hotels. 
It is divided in two modules: feature selection and ranking. 


###How it works:
1) Install docker

2) Download the datasets:

https://recsys.trivago.cloud/

3) Build docker:

 `cd feature_selection`

`docker build -t "recsyschallenge" .`

and

 `cd CF`

`docker build -t "recsyschallenge" .`

4) For feature selection run: 

`docker run --volume=/path/for/directory/with/data:/Data  --volume=~/cf4tourism/feature_selection/src:/src_ -e testpath=/Data/test.csv recsyschallenge`


Datasets with test features and train features are available at: 
https://drive.google.com/drive/folders/1d_W8MXTgraWg5zOjVLXwskQum4Y4zAHS?usp=sharing

5) For prediction run:  


`docker run --volume=/path/for/directory/with/data:/Data  --volume=~/cf4tourism/CF/src:/src_ -e testpath=/Data/test.csv recsyschallenge`

This step is not optimized, so we give the hypothesis of dividing the test set into parts and running several parts at the same time.

Run the command in 5 for each file, altering the name in testpath=/Data/test.csv. For example, if the file is called test_0.csv, it will be testpath=/Data/test_0.csv.

The submission file is stored into a file called test.csv.csv (test_0.csv.csv).

You can use this code to separe the test csv: 
https://bitbucket.org/snippets/mcabarros/Mexaoo/separate-csv

And this code to put the submissions csv's together:

https://bitbucket.org/snippets/mcabarros/Bexxpr/merge-csvs-in-directory



 