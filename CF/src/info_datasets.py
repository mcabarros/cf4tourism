import pandas as pd
import numpy as np
import sys


def get_indexes_for_session(list_of_sessionIDs, matrix_of_indices):
    matrix_of_indices = matrix_of_indices

    matrix_of_indices = pd.DataFrame(matrix_of_indices, columns=['IDs', "indices"])
    matrix_of_indices = matrix_of_indices.applymap(
        lambda x: x.decode() if isinstance(x, bytes) else x)  # byte array to string

    list_of_indices = matrix_of_indices[matrix_of_indices.IDs.isin(list_of_sessionIDs)].indices

    indices = list_of_indices.values.flatten()

    return list(map(int, indices))


def get_all_session(dataset, id):
    all_session = dataset[dataset.session_id == id]

    return all_session


def get_part_of_df_by_session_id(df, selection):
    df = df[df.session_id.isin(selection)]

    return df


def get_clickouts(df):
    mask = df["action_type"] == "clickout item"
    df_clicks = df[mask].reference

    return df_clicks
