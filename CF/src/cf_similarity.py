import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from scipy.spatial.distance import pdist
from scipy.spatial.distance import cdist
from scipy.spatial.distance import squareform
from sklearn import preprocessing
from scipy import sparse
import pandas as pd


def getSimilarityMatrixCdist(test, train, simType):
    '''
    uses cosine similarity to calculate similarity between rows in a dataset
    :param df: normalized array
    :return: numpy array of similarities mSourcesxMsources
    '''

    similarities = 1 - cdist(test, train, metric=simType) #pdist(df, metric='cosine') = distance; 1-pdist(df, metric='cosine') = similarity

    if simType=='euclidean':
        similarities = (similarities-np.amin(similarities))/(np.amax(similarities)-np.amin(similarities))

    #similarities = squareform(similarities)


    return similarities

def get_percentage_of_most_similar(df_sim):

    df_sim = df_sim.sort_values(by=['sim'], ascending=False)

    x, y = df_sim.shape

    n = np.ceil(x*1).astype(int)


    df_sim = df_sim.head(n)


    return df_sim


