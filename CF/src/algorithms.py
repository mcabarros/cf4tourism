from functions import *
from info_datasets import *
from cf_similarity import *
import csv
import os
import numpy as np


def cf_by_city_out_position(test_csv, df_test, df_train, df_features_test, df_features_train):
    '''
    collaborative filtering algorithm. It selects all in the train by the city, and then it calculates how may
    sessions chose the position, 0, 1, 2...till 24. Then it orders the impressions of the test by these values.
    :param test_csv:
    :param df_test:
    :param df_train:
    :param df_features_test:
    :param df_features_train:
    :return:
    '''

    with open(test_csv + ".csv", "w", newline='') as f:
        # item_by_clusters = get_all_clusters_ids(item_by_cluster_path)

        test_target = get_submission_target(df_test)

        header = ['user_id', 'session_id', 'timestamp', 'step', 'item_recommendations']
        writer = csv.writer(f, delimiter=',')
        writer.writerow(header)

        for i, row in test_target.iterrows():
            print(row.session_id)
            print(i)

            test_impressions_array = string_to_array(row.impressions)

            test_impressions_df = pd.DataFrame(np.array(test_impressions_array).astype(int), columns=["impressions"])

            # test_impressions_df[output_columns] = test_impressions_df['impressions'].map(
            #    item_by_clusters.set_index('item_id')[output_columns])

            test_impressions_df["click_out_position"] = np.array(range(0, len(test_impressions_array)))

            sys.stdout.flush()
            city = row.city  # get city for click out

            train_by_test_city = df_train[df_train.city == city]  # get all sessions from train for that city
            train_by_test_city_unique_session = train_by_test_city.session_id.unique().tolist()  # get unique sessions IDs - this is the list of ids to calculate the similarity for this session

            train_features_by_city = df_features_train[
                df_features_train.session_id.isin(train_by_test_city_unique_session)]

            train_features_by_city = train_features_by_city.dropna()  # drop rows with any nan

            train_features_by_city_for_sim = train_features_by_city.drop(
                ["session_id", "click_out_position"], axis=1)

            this_test_features = df_features_test[df_features_test.session_id == row.session_id]

            this_test_features = this_test_features.drop(["session_id"], axis=1)

            similarity_list = getSimilarityMatrixCdist(this_test_features, train_features_by_city_for_sim, "cosine")

            train_features_by_city["sim"] = similarity_list[0]

            df_sim = get_percentage_of_most_similar(train_features_by_city)  # get the x% most similar

            # df_sim_grouped_by_cluster = pd.DataFrame(df_sim.groupby(output_columns).size(),
            #                                         columns=["countN"])

            df_sim_grouped_by_position = pd.DataFrame(df_sim.groupby("click_out_position").size(),
                                                      columns=["countN"])

            # test_impressions_df["order_cluster"] = test_impressions_df[output_columns].map(
            #    df_sim_grouped_by_cluster["countN"])

            test_impressions_df["order_position"] = test_impressions_df["click_out_position"].map(
                df_sim_grouped_by_position["countN"])

            # print(np.array(df_sim.groupby("click_out_position").size().click_out_position))

            # teste = aaa.reindex(np.arange(0, 25)).fillna(0)

            # test_impressions_df["order"] = np.array(teste.countN)

            # test_impressions_df["order"] = np.array(df_sim.groupby("click_out_position").size())

            # test_impressions_array_sorted = test_impressions_df.sort_values(by="order", ascending=False)

            test_impressions_array_sorted = test_impressions_df.sort_values(by="order_position", ascending=False)

            a = np.array(test_impressions_array_sorted.impressions).astype(str).flatten().tolist()

            rec = [row.user_id, row.session_id, row.timestamp, row.step, ' '.join(a)]

            writer.writerow(rec)
            f.flush()
            sys.stdout.flush()



