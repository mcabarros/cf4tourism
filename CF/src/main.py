from algorithms import *
import csv
import pandas as pd

from os import listdir

'''
collaborative filtering algorithm
'''


def main():
    test_csv = sys.argv[1]

    train_csv = "/Data/train.csv"

    train_features_path = "/Data/train_features.csv"
    test_features_path = "/Data/test_features.csv"

    df_train = pd.read_csv(train_csv)

    df_test = pd.read_csv(test_csv)

    df_features_train = pd.read_csv(train_features_path)
    df_features_test = pd.read_csv(test_features_path)

    cf_by_city_out_position(test_csv, df_test, df_train, df_features_test, df_features_train)


if __name__ == '__main__':
    main()
