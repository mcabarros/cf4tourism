import pandas as pd
import numpy as np
import sys


def get_all_session(dataset, id):
    all_session = dataset[dataset.session_id == id]

    return all_session


def action_string_to_num(dataset):
    '''
    Receives a pandas dataset and change the string column "action type" to a number,
    depending on the action
    :param dataset: pandas dataset
    :return: pandas dataset
    '''

    dataset.loc[dataset['action_type'] == "clickout item", 'action_type'] = 10
    dataset.loc[dataset['action_type'] == "interaction item rating", 'action_type'] = 2
    dataset.loc[dataset['action_type'] == "interaction item info", 'action_type'] = 3
    dataset.loc[dataset['action_type'] == "interaction item image", 'action_type'] = 4
    dataset.loc[dataset['action_type'] == "interaction item deals", 'action_type'] = 5
    dataset.loc[dataset['action_type'] == "change of sort order", 'action_type'] = 6
    dataset.loc[dataset['action_type'] == "filter selection", 'action_type'] = 7
    dataset.loc[dataset['action_type'] == "search for item", 'action_type'] = 8
    dataset.loc[dataset['action_type'] == "search for destination", 'action_type'] = 9
    dataset.loc[dataset['action_type'] == "search for poi", 'action_type'] = 1

    return dataset


def insert_data_to_memmap(dataset, matrix, i, index_min, index_max, index_matrix):

    dataset = dataset[dataset.session_id.isin(i.values.flatten())]

    print("dataset_shape: ", dataset.shape)

    g = dataset.groupby("session_id").cumcount()

    L_2 = dataset.set_index(['session_id', g]).unstack(fill_value=0).stack().groupby(level=0).apply(
        lambda x: x.values)

    L_2_array_np = np.array(L_2)

    y_shape = L_2_array_np[0].flatten().shape[0] # get the size in y for the array of features



    #inserts features into npy matrix
    matrix[index_min:index_max, 0:y_shape] = np.resize(np.concatenate(L_2_array_np).ravel(), (len(i), y_shape))


    indices = np.arange(index_min, index_max)
    IDs = pd.DataFrame(L_2.index)
    IDs["indices_of_the_matrix"] = indices

    # inserts relation of index in the matrix of features with session_id
    index_matrix[index_min:index_max] = np.array(IDs)


def define_index(index_min, index_max, step):
    index_max = index_min + step

    return index_min, index_max


def create_features_matrix(matrix, dataset, index_matrix):
    '''
    creates a npy file with a list of features for each session
    Each feature is coded to a number

    :param matrix: npy matrix
    :param dataset: pandas dataset
    :return:
    '''

    print(dataset.head(2))
    dataset = action_string_to_num(dataset)
    # print(np.array(dataset.groupby(['session_id'])['action_type'].unique().head(5)))

    new_dataset = dataset[['session_id', 'action_type']]

    unique_sessions = dataset.session_id.unique()
    index_min = 0
    index_max = 0

    for i in np.array_split(pd.DataFrame(unique_sessions), 1000):
        index_min, index_max = define_index(index_min, index_max, len(i))
        print(index_min, index_max)
        insert_data_to_memmap(new_dataset, matrix, i, index_min, index_max, index_matrix)

        index_min = index_max

        sys.stdout.flush()


    sys.stdout.flush()
