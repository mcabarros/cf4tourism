from functions_features import *
from create_features_matrix import *
import os
import numpy as np
import pandas as pd

'''
File to create features matrices for test and train set
'''


def count_unique_filters(test_filters):
    list_of_test_filters = []

    count2 = 0
    for test_filt in test_filters:

        if isinstance(test_filt, str):
            a = string_to_array(test_filt)

            list_of_test_filters.append(a)

        count2 += 1

    test_filters_flattened = np.array([val for sublist in list_of_test_filters for val in sublist])

    a, count = np.unique(test_filters_flattened, return_counts=True)

    return a


def get_columns_list(df_train, df_test):
    test_filters = np.array(df_test.current_filters)

    train_filters = np.array(df_train.current_filters)

    all_filters = np.concatenate((test_filters, train_filters), axis=None)

    list_of_filters = count_unique_filters(all_filters)

    return list_of_filters


def get_not_in_list(df1, df2):
    df1_filters = np.array(df1.current_filters)
    df2_filters = np.array(df2.current_filters)

    df1_list_of_filters = count_unique_filters(df1_filters)
    df2_list_of_filters = count_unique_filters(df2_filters)

    mask = np.isin(df1_list_of_filters, df2_list_of_filters)

    df1_not_in_df2 = df1_list_of_filters[~mask]

    return df1_not_in_df2


def create_features_csv(df, extra_columns):
    session_steps = pd.DataFrame(df.groupby("session_id").step.size())
    # print(df_train.groupby("session_id", "action_type").count())
    session_filter_action_type = df.groupby(['session_id', 'action_type']).size().unstack(0).fillna(0).T

    exploded_df_train = explode(df, "current_filters")

    session_filter_matrix = exploded_df_train.groupby(['session_id', 'current_filters']).size().unstack(0).fillna(0).T

    total = session_filter_matrix.merge(session_filter_action_type, how='outer', left_index=True,
                                        right_index=True).fillna(0)
    total = total.merge(session_steps, how='outer', left_index=True, right_index=True).fillna(0)
    x, y = total.shape

    total = total.drop(columns=extra_columns)

    # for col in extra_columns:
    #    total[col] = np.zeros(x)

    return total


def clickout_position(df_train, train_features):
    idx = df_train.groupby(['session_id'])['step'].transform(max) == df_train['step']

    # df_train_max_step = df_train.groupby(["session_id"], sort=False)["step"].max()
    df_train_max_step = df_train[idx]

    df_train_max_step_clickout = df_train_max_step[df_train_max_step.action_type == "clickout item"]

    df_train_max_step_clickout_exploded = explode_int(df_train_max_step_clickout, "impressions")

    df_train_max_step_clickout_exploded_ = df_train_max_step_clickout_exploded[
        ["reference", "impressions"]]
    df_train_max_step_clickout_exploded_.reference = df_train_max_step_clickout_exploded_.reference.astype(int)

    df_train_max_step_clickout_exploded_ = df_train_max_step_clickout_exploded_.where(
        df_train_max_step_clickout_exploded_.reference == df_train_max_step_clickout_exploded_.impressions).fillna(0)

    df_train_max_step_clickout_exploded_["session_id"] = df_train_max_step_clickout_exploded.session_id

    df_session_id = pd.DataFrame(
        np.array(df_train_max_step_clickout_exploded_[df_train_max_step_clickout_exploded_.reference > 0].session_id),
        columns=["session_id"])

    indexing_impresstions = np.array(df_train_max_step_clickout_exploded_.groupby("session_id").impressions.cumcount())

    impressions = np.array(df_train_max_step_clickout_exploded_.impressions)

    df_session_id["click_out_position"] = np.array(indexing_impresstions[np.where(impressions > 0)])

    train_features = train_features.merge(df_session_id.set_index("session_id"), how='outer', left_index=True,
                                          right_index=True)  # .fillna(0)

    return train_features


def all_filters(df_train, df_test):
    '''
    Creates datasets with the features of the train and the test for each unique session.
    The features are based on the filters and on the action types.
    The last column of the train_features.csv is the position of the clickout
    :param df_train: train set
    :param df_test: test set
    :return:
    '''


    path_train = "/Data/train_features.csv"
    path_test = "/Data/test_features.csv"

    train_filters_not_in_test = get_not_in_list(df_train, df_test)

    test_filters_not_in_train = get_not_in_list(df_test, df_train)

    train_features = create_features_csv(df_train, train_filters_not_in_test)
    test_features = create_features_csv(df_test, test_filters_not_in_train)

    test_features = test_features.reindex(train_features.columns, axis=1)

    test_features.to_csv(path_test, sep=',')

    train_features = clickout_position(df_train, train_features)

    train_features.to_csv(path_train, sep=',', index_label="session_id")


def main():
    train_csv = "/Data/train.csv"
    test_csv = "/Data/test.csv"

    # fp2 = np.load(articlesSharing_path, mmap_mode='r+')

    print(f"Reading {train_csv} ...")
    df_train = pd.read_csv(train_csv)
    print(f"Reading {test_csv} ...")
    df_test = pd.read_csv(test_csv)

    print("train size: ", df_train.shape)
    print("train size: ", df_train.columns)
    print("train unique sessions: ", df_train.session_id.unique().size)
    print("train unique users: ", df_train.user_id.unique().shape)

    print("teste size: ", df_test.shape)
    print("test unique sessions: ", df_test.session_id.unique().shape)
    print("test unique user: ", df_test.user_id.unique().shape)

    print("max step test :", df_test.step.max())
    print("max step train :", df_train.step.max())

    all_filters(df_train, df_test)

    print("finish")


if __name__ == '__main__':
    main()
